<?php


namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $device = new Device();
            $device->setHostname($faker->unique()->word);
            $device->setLoopback($faker->unique()->ipv4);
            $manager->persist($device);
            $manager->flush();
            for ($j = 0; $j < $faker->numberBetween(5, 10); $j++) {
                $deviceInterface = new DeviceInterface();
                $deviceInterface->setName($faker->unique()->word);
                $deviceInterface->setLoopback($faker->unique()->ipv4);
                $deviceInterface->setDevice($device);
                $manager->persist($deviceInterface);
            }
            $manager->flush();
        }
    }
}
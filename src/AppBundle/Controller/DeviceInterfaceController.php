<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DeviceInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DeviceInterfaceController extends Controller
{
    /**
     * @Route("/interface/{id}")
     */
    public function showAction($id)
    {
        $interfaces = $this->getDoctrine()
            ->getRepository('AppBundle:DeviceInterface')
            ->findBy(['device' => $id]);

        $data = ['interfaces' => []];
        foreach ($interfaces as $interface) {
            $data['interfaces'][] = $this->serializeInterface($interface);
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/interface/new/{id}", name="deviceInterface_new")
     */
    public function newAction(Request $request, $id)
    {
        $device = $this->getDoctrine()
            ->getRepository('AppBundle:Device')
            ->findOneBy(['id' => $id]);

        $deviceInterface = new DeviceInterface();
        if ($request->get('name')) {
            for ($i = 0; $i < count($request->get('name')); $i++) {
                $deviceInterface->setName($request->get('name')[$i]);
                $deviceInterface->setLoopback($request->get('iloopback')[$i]);
                $deviceInterface->setDevice($device);
                $em = $this->getDoctrine()->getManager();
                $em->persist($deviceInterface);
                $em->flush();
            }
            $this->addFlash('success', 'Interface Created!!');
            return $this->redirectToRoute('device_list');
        }

        return $this->render('deviceInterface/new.html.twig', [
            'id' => $id
        ]);
    }

    public function serializeInterface(DeviceInterface $deviceInterface) {
        return [
            'id' => $deviceInterface->getId(),
            'name' => $deviceInterface->getName(),
            'loopback' => $deviceInterface->getLoopback()
        ];
    }
}

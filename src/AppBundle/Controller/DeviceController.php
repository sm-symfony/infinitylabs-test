<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceInterface;
use Faker\Factory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DeviceController extends Controller
{
    /**
     * @Route("/devices", name="device_list")
     */
    public function listAction()
    {
        $devices = $this->getDoctrine()
            ->getRepository('AppBundle:Device')
            ->findAll();
        return $this->render('device/list.html.twig', [
            'devices' => $devices
        ]);
    }

    /**
     * @Route("/devices/new", name="device_new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $device = new Device();

//        $faker = Factory::create();
//        $deviceInterface1 = new DeviceInterface();
//        $deviceInterface1->setName($faker->unique()->word);
//        $deviceInterface1->setLoopback($faker->unique()->ipv4);
//        $device->getDeviceInterfaces()->add($deviceInterface1);
//
//        $deviceInterface2 = new DeviceInterface();
//        $deviceInterface2->setName($faker->unique()->word);
//        $deviceInterface2->setLoopback($faker->unique()->ipv4);
//        $device->getDeviceInterfaces()->add($deviceInterface2);

        $deviceForm = $this->createForm('AppBundle\Form\DeviceFormType', $device);
        $deviceForm->handleRequest($request);
        if ($deviceForm->isSubmitted() && $deviceForm->isValid()) {
            $device = $deviceForm->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($device);
            $em->flush();
            $this->addFlash('success', 'Device Created!!');
            return $this->redirectToRoute('device_list');
        }

        return $this->render('device/new.html.twig', [
            'deviceForm' => $deviceForm->createView()
        ]);
    }
}
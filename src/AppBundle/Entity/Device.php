<?php


namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="device")
 * @UniqueEntity("hostname")
 * @UniqueEntity("loopback")
 */
class Device
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank(message="Hostname can not be blank.")
     */
    private $hostname;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank(message="Loopback can not be blank.")
     */
    private $loopback;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DeviceInterface", mappedBy="device", cascade={"persist"})
     */
    private $deviceInterfaces;

    public function __construct()
    {
        $this->deviceInterfaces = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getHostname()
    {
        return $this->hostname;
    }

    /**
     * @param string $hostname
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;
    }

    /**
     * @return string
     */
    public function getLoopback()
    {
        return $this->loopback;
    }

    /**
     * @param string $loopback
     */
    public function setLoopback($loopback)
    {
        $this->loopback = $loopback;
    }

    /**
     * @param DeviceInterface $deviceInterfaces
     */
    public function setDeviceInterfaces($deviceInterfaces)
    {
        $this->deviceInterfaces = $deviceInterfaces;
    }

    /**
     * @return ArrayCollection|DeviceInterface[]
     */
    public function getDeviceInterfaces()
    {
        return $this->deviceInterfaces;
    }

    public function addDeviceInterface(DeviceInterface $deviceInterface)
    {
        $deviceInterface->setDevice($this);
        $this->deviceInterfaces->add($deviceInterface);
    }

    public function removeDeviceInterface(DeviceInterface $deviceInterface)
    {
        $this->deviceInterfaces->removeElement($deviceInterface);
    }
}
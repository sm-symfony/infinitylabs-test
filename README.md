router
======

A Symfony project created on June 29, 2018, 9:10 am.

1. Change database config here router\app\config\parameters.yml
2. ``composer install``
3. ``./bin/console doctrine:database:create``
4. ``./bin/console doctrine:migrations:migrate``
5. ``./bin/console doctrine:fixtures:load``
6. ``./bin/console server:run``
7. Check output at http://localhost:8000
const showInterface = document.querySelector('.show-interface');

if (showInterface) {
    document.body.addEventListener('click', function (e) {
        if (e.target.classList.contains('show-interface')) {
            fetch(`/interface/${e.target.id}`)
            .then(function (res) {
                return res.json();
            })
            .then(data => {
                console.log(data.interfaces);
                let output = `<table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>loopback</th>
                            </tr>
                            </thead>
                            <tbody>`;
                data.interfaces.forEach(interface => {
                    output += `<tr>
                            <th scope="row">${interface.id}</th>
                            <td>${interface.name}</a></td>
                            <td>${interface.loopback}</td>
                        </tr>`;
                });
                output += `</tbody></table>`;
                document.querySelector('.modal-content').innerHTML = output;
            })
            .catch(err => {
                console.log(err);
            });
        }
    });
}